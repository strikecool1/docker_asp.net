# Образ sdk
FROM mcr.microsoft.com/dotnet/sdk:3.1

WORKDIR /var/www/html/asp

ENV DOTNET_USE_POLLING_FILE_WATCHER 1

COPY . /var/www/html/asp

WORKDIR /var/www/html/asp

RUN ["dotnet", "restore"]

RUN ["dotnet", "build"]

ENTRYPOINT dotnet watch run --no-restore --urls=http://+:5000