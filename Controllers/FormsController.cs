﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Dashboard.Controllers
{
    public class FormsController : Controller
    {
        [Route("Forms")]
        public IActionResult Forms()
        {
            return View("~/Views/Forms.cshtml");
        }
    }
}
