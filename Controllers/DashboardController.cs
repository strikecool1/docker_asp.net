﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Dashboard.Controllers
{
    public class DashboardController : Controller
    {
        [Route("/")]
        public IActionResult Dashboard()
        {
            return View("~/Views/Index.cshtml");
        }
    }
}
