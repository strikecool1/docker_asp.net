﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Dashboard.Controllers
{
    public class CardsController : Controller
    {
        [Route("Cards")]
        public IActionResult Cards()
        {
            return View("~/Views/Cards.cshtml");
        }
    }
}
