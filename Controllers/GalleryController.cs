﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Dashboard.Controllers
{
    public class GalleryController : Controller
    {
        [Route("Gallery")]
        public IActionResult Gallery()
        {
            return View("~/Views/Gallery.cshtml");
        }
    }
}
