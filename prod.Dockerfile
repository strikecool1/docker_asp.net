# Для релиза 
FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app

# Образ с DockerHub
FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["ASP_Dashboard.csproj", "."]
RUN dotnet restore "./ASP_Dashboard.csproj"
# Копируем все файлы проекта в контейнер
COPY . .
WORKDIR "/src/."
RUN dotnet build "ASP_Dashboard.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ASP_Dashboard.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /var/www/html/asp/bin
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ASP_Dashboard.dll"]